#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pathlib import Path

from setuptools import setup


def parse_requirements():
    path = Path(__file__).parent.resolve() / "requirements.txt"
    assert path.exists(), f"Missing requirements: {path}"
    return list(map(str.strip, path.read_text().splitlines()))


setup(
    name="ArkindexDocumentLayoutTrainingLabelNormalization",
    version=open("VERSION").read(),
    description="Tool to extract data from Arkindex, prepare and normalize it for image segmentation algorithms",
    author="Mélodie Boillet",
    author_email="boillet@teklia.com",
    install_requires=parse_requirements(),
    entry_points={"console_scripts": ["run-extraction=extractor.extract:main"]},
    packages=["extractor", "tests"],
    long_description=open("README.md").read(),
)
