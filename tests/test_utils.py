#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
from argparse import Namespace

import pytest
from shapely.geometry import Polygon

from extractor import utils


@pytest.mark.parametrize(
    "input_str, expected_output",
    [
        ("0,0,0", [0, 0, 0]),
        ("0, 0, 0", [0, 0, 0]),
        ("127,50,0", [127, 50, 0]),
    ],
)
def test_color_string_valid(input_str, expected_output):
    assert utils.color_string(input_str) == expected_output


@pytest.mark.parametrize(
    "input_str, expected_error",
    [
        ("0", "Invalid color [0], should have 3 values"),
        ("0,0,0,0", "Invalid color [0, 0, 0, 0], should have 3 values"),
        ("0,-50", "Invalid color [0, -50], should have 3 values"),
        ("0,0,-50", "Invalid color [0, 0, -50], values should be between 0 and 255"),
        ("0,0,300", "Invalid color [0, 0, 300], values should be between 0 and 255"),
    ],
)
def test_color_string_invalid(input_str, expected_error):
    with pytest.raises(AssertionError, match=re.escape(expected_error)):
        utils.color_string(input_str)


@pytest.mark.parametrize(
    "args",
    [
        (Namespace(image_size=50, classes=["bg"], colors=[(0, 0, 0)])),
        (
            Namespace(
                image_size=50, classes=["bg", "tl"], colors=[(0, 0, 0), (0, 0, 255)]
            )
        ),
    ],
)
def test_check_cli_args_valid(args):
    assert utils.check_cli_args(args) == args


@pytest.mark.parametrize(
    "args, expected_error",
    [
        (
            Namespace(
                image_size=-10, classes=["bg", "tl"], colors=[(0, 0, 0), (0, 0, 255)]
            ),
            "Invalid image_size, should be a positive integer",
        ),
        (
            Namespace(
                image_size=0, classes=["bg", "tl"], colors=[(0, 0, 0), (0, 0, 255)]
            ),
            "Invalid image_size, should be a positive integer",
        ),
        (
            Namespace(image_size=50, classes=[], colors=[(0, 0, 0), (0, 0, 255)]),
            "Invalid number of classes, should have at least 1 class",
        ),
        (
            Namespace(image_size=50, classes=[], colors=[]),
            "Invalid number of classes, should have at least 1 class",
        ),
        (
            Namespace(image_size=50, classes=["bg"], colors=[(0, 0, 0), (0, 0, 255)]),
            "Invalid number of colors, should have one color by class",
        ),
        (
            Namespace(image_size=50, classes=["bg"], colors=[]),
            "Invalid number of colors, should have one color by class",
        ),
        (
            Namespace(image_size=50, classes=["bg", "tl"], colors=[(0, 0, 0)]),
            "Invalid number of colors, should have one color by class",
        ),
    ],
)
def test_check_cli_args_invalid(args, expected_error):
    with pytest.raises(AssertionError, match=re.escape(expected_error)):
        utils.check_cli_args(args)


@pytest.mark.parametrize(
    "polygons, ratio_h, ratio_w, expected_output",
    [
        (
            [Polygon([(10, 10), (20, 20), (20, 10)])],
            1,
            1,
            [Polygon([(10, 10), (20, 20), (20, 10)])],
        ),
        (
            [Polygon([(10, 10), (20, 20), (20, 10)])],
            0,
            0,
            [Polygon([(0, 0), (0, 0), (0, 0)])],
        ),
        (
            [Polygon([(10, 10), (20, 20), (20, 10)])],
            2,
            2,
            [Polygon([(20, 20), (40, 40), (40, 20)])],
        ),
        (
            [Polygon([(10, 10), (20, 20), (20, 10)])],
            0.5,
            0.5,
            [Polygon([(5, 5), (10, 10), (10, 5)])],
        ),
        (
            [
                Polygon([(10, 10), (20, 20), (20, 10)]),
                Polygon([(25, 15), (26, 30), (28, 19)]),
            ],
            0.5,
            0.5,
            [
                Polygon([(5, 5), (10, 10), (10, 5)]),
                Polygon([(12.5, 7.5), (13, 15), (14, 9.5)]),
            ],
        ),
        (
            [Polygon([(10, 10), (20, 20), (20, 10)])],
            2,
            0.5,
            [Polygon([(5, 20), (10, 40), (10, 20)])],
        ),
        (
            [
                Polygon([(10, 10), (20, 20), (20, 10)]),
                Polygon([(25, 15), (26, 30), (28, 19)]),
            ],
            0.5,
            2,
            [
                Polygon([(20, 5), (40, 10), (40, 5)]),
                Polygon([(50, 7.5), (52, 15), (56, 9.5)]),
            ],
        ),
    ],
)
def test_resize_polygons(polygons, ratio_h, ratio_w, expected_output):
    assert utils.resize_polygons(polygons, ratio_h, ratio_w) == expected_output


@pytest.mark.parametrize(
    "polygons, expected_output",
    [
        # 1 polygon: only erosion
        ([Polygon([(10, 10), (20, 20), (20, 10)])], [Polygon([])]),
        (
            [Polygon([(20, 20), (40, 40), (40, 20)])],
            [
                Polygon(
                    [
                        (29.656854249492383, 24.0),
                        (36.0, 30.343145750507617),
                        (36.0, 24.0),
                    ]
                )
            ],
        ),
        # 2 polygons at the same position (overlap > 20%): only erosion
        (
            [
                Polygon([(20, 20), (40, 40), (40, 20)]),
                Polygon([(20, 20), (40, 40), (40, 20)]),
            ],
            [
                Polygon(
                    [
                        (29.656854249492383, 24.0),
                        (36.0, 30.343145750507617),
                        (36.0, 24.0),
                    ]
                ),
                Polygon(
                    [
                        (29.656854249492383, 24.0),
                        (36.0, 30.343145750507617),
                        (36.0, 24.0),
                    ]
                ),
            ],
        ),
        # 2 touching polygons: only erosion
        (
            [
                Polygon([(20, 20), (40, 40), (40, 20)]),
                Polygon([(40, 20), (40, 40), (60, 60)]),
            ],
            [
                Polygon(
                    [
                        (29.656854249492383, 24.0),
                        (36.0, 30.343145750507617),
                        (36.0, 24.0),
                        (29.656854249492383, 24.0),
                    ]
                ),
                Polygon(
                    [
                        (44.0, 36.944271909999166),
                        (44.0, 38.34314575050762),
                        (45.39887384050845, 39.74201959101607),
                        (44.0, 36.944271909999166),
                    ]
                ),
            ],
        ),
        (
            [
                Polygon([(20, 20), (40, 40), (40, 20)]),
                Polygon([(40, 20), (40, 40), (50, 50)]),
            ],
            [
                Polygon(
                    [
                        (29.656854249492383, 24.0),
                        (36.0, 30.343145750507617),
                        (36.0, 24.0),
                        (29.656854249492383, 24.0),
                    ]
                ),
                Polygon([]),
            ],
        ),
        # 2 overlapping polygons: remove intersection + erosion
        (
            [
                Polygon([(20, 20), (40, 40), (40, 20)]),
                Polygon([(39, 20), (39, 39), (60, 60)]),
            ],
            [
                Polygon(
                    [
                        (29.656854249492383, 24.0),
                        (36.0, 30.343145750507617),
                        (36.0, 24.0),
                        (29.656854249492383, 24.0),
                    ]
                ),
                Polygon(
                    [
                        (43.0, 36.22427199853449),
                        (43.0, 37.34314575050762),
                        (44.23664993639135, 38.57979568689897),
                        (43.0, 36.22427199853449),
                    ]
                ),
            ],
        ),
    ],
)
def test_split_polygons(polygons, expected_output):
    assert utils.split_polygons(polygons) == expected_output
