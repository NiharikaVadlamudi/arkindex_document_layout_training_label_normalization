#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    The arkindex_utils module
    ======================
"""

import errno
import logging
import sys

from apistar.exceptions import ErrorResponse


def retrieve_corpus(client, corpus_name: str) -> str:
    """
    Retrieve the corpus id from the corpus name.
    :param client: The arkindex client.
    :param corpus_name: The name of the corpus to retrieve.
    :return target_corpus: The id of the retrieved corpus.
    """
    for corpus in client.request("ListCorpus"):
        if corpus["name"] == corpus_name:
            target_corpus = corpus["id"]
    try:
        logging.info(f"Corpus id retrieved: {target_corpus}")
    except NameError:
        logging.error(f"Corpus {corpus_name} not found")
        sys.exit(errno.EINVAL)

    return target_corpus


def retrieve_subsets(
    client, corpus: str, parents_types: list, parents_names: list
) -> list:
    """
    Retrieve the requested subsets.
    :param client: The arkindex client.
    :param corpus: The id of the retrieved corpus.
    :param parents_types: The types of parents of the elements to retrieve.
    :param parents_names: The names of parents of the elements to retrieve.
    :return subsets: The retrieved subsets.
    """
    subsets = []
    for parent_type in parents_types:
        try:
            subsets.extend(
                client.request("ListElements", corpus=corpus, type=parent_type)[
                    "results"
                ]
            )
        except ErrorResponse as e:
            logging.error(f"{e.content}: {parent_type}")
            sys.exit(errno.EINVAL)
    # Retrieve subsets with name in parents-names. If no parents-names given, keep all subsets.
    if parents_names is not None:
        logging.info(f"Retrieving {parents_names} subset(s)")
        subsets = [subset for subset in subsets if subset["name"] in parents_names]
    else:
        logging.info("Retrieving all subsets")

    if len(subsets) == 0:
        logging.info("No subset found")

    return subsets


def retrieve_elements(client, page_id: str):
    """
    Retrieve the elements from arkindex of a given page.
    :param client: The arkindex client.
    :param page_id: The id of the page to retrieve the elements.
    :return: The retrieved elements.
    """
    return client.paginate(
        "ListElementChildren", id=page_id, recursive=True, page_size=200
    )
