#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    The extraction module
    ======================
"""

import logging
import os

import cv2
import imageio as iio
import numpy as np
from arkindex import ArkindexClient, options_from_env
from shapely.geometry import Polygon
from tqdm import tqdm

from extractor import arkindex_utils as ark
from extractor import utils
from extractor import utils_image as ui

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


IMAGES_DIR = "./images/"  # Path to the images directory.
LABEL_IMAGES_DIR = "./labels_images/"  # Path to the label images directory.
LABEL_JSON_DIR = "./labels_json/"  # Path to the json labels directory.


def main():
    args = utils.get_cli_args()

    # Get and initialize the parameters.
    os.makedirs(IMAGES_DIR, exist_ok=True)
    os.makedirs(LABEL_IMAGES_DIR, exist_ok=True)
    os.makedirs(LABEL_JSON_DIR, exist_ok=True)

    # Login to arkindex.
    client = ArkindexClient(**options_from_env())

    corpus = ark.retrieve_corpus(client, args.corpus)
    subsets = ark.retrieve_subsets(
        client, corpus, args.parents_types, args.parents_names
    )

    # Iterate over the subsets to find the page images and labels.
    for subset in subsets:

        os.makedirs(os.path.join(IMAGES_DIR, subset["name"]), exist_ok=True)
        os.makedirs(os.path.join(LABEL_IMAGES_DIR, subset["name"]), exist_ok=True)
        os.makedirs(os.path.join(LABEL_JSON_DIR, subset["name"]), exist_ok=True)

        for page in tqdm(
            client.paginate(
                "ListElementChildren", id=subset["id"], type="page", recursive=True
            ),
            desc="Set " + subset["name"],
        ):

            # Get the label size and initialize the label image.
            if args.image_size is not None:
                ann_h, ann_w, ratio_h, ratio_w = ui.new_image_size(
                    [page["zone"]["image"]["height"], page["zone"]["image"]["width"]],
                    args.image_size,
                )
                label_image = np.zeros((ann_h, ann_w, 3), dtype=np.uint8)
            else:
                label_image = np.zeros(
                    (
                        page["zone"]["image"]["height"],
                        page["zone"]["image"]["width"],
                        3,
                    ),
                    dtype=np.uint8,
                )

            # Get the label size and initialize the json label.
            page_objects = {}
            page_objects["img_size"] = [
                int(page["zone"]["image"]["height"]),
                int(page["zone"]["image"]["width"]),
            ]

            # Retrieve the polygons from arkindex.
            elements = ark.retrieve_elements(client, page["id"])
            for index, channel in enumerate(args.classes):
                # Filter polygons of current channel.
                polygons = [
                    element["zone"]["polygon"]
                    for element in elements
                    if element["type"] == channel
                ]

                # Add the polygons to dict.
                page_objects[channel] = [
                    {
                        "confidence": 1.0,
                        "polygon": polygon,
                    }
                    for polygon in polygons
                ]

                # Prepare polygons for label image.
                polygons = [Polygon(poly) for poly in polygons]
                # Resize the polygons if requested.
                if args.image_size is not None:
                    polygons = utils.resize_polygons(polygons, ratio_h, ratio_w)
                # Split the polygons if requested.
                if args.split_process:
                    polygons = utils.split_polygons(polygons)

                label_image = ui.update_image(label_image, polygons, args.colors[index])

            # Save the input image.
            image = iio.imread(page["zone"]["url"])
            cv2.imwrite(
                os.path.join(IMAGES_DIR, subset["name"], f"{page['name']}.png"),
                cv2.cvtColor(image, cv2.COLOR_BGR2RGB),
            )
            # Save the labels.
            cv2.imwrite(
                os.path.join(LABEL_IMAGES_DIR, subset["name"], f"{page['name']}.png"),
                cv2.cvtColor(label_image, cv2.COLOR_BGR2RGB),
            )
            utils.save_polygons(
                page_objects,
                os.path.join(LABEL_JSON_DIR, subset["name"], f"{page['name']}.json"),
            )
