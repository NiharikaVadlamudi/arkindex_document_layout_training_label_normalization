# Arkindex Document Layout Training Label Normalization

## A tool to extract data from Arkindex, prepare and normalize it for image segmentation algorithms

This tool extracts and prepares datasets from Arkindex to train and evaluate image segmentation algorithms. It extracts the images, annotation images and annotations in json format. The annotations in json format can be directly used for evaluation using the [Document Image Segmentation Scoring](https://gitlab.com/teklia/dla/document_image_segmentation_scoring) tool.

The annotation images can be retrieved in a specific size and a pre-processing can be applied to the polygons to prevent them from overlapping. The whole process is described in the paper entitled "Robust Text Line Detection in Historical Documents: Learning and Evaluation Methods".

## Cloning and installation

```console
git clone https://gitlab.com/teklia/dla/arkindex_document_layout_training_label_normalization.git
cd arkindex_document_layout_training_label_normalization
pip install -e .
```

## Usage

To start extraction from Arkindex, use the following command:
```console
run-extraction --corpus <arkindex_corpus_name>
  --classes <class1> <class2> ...
  --colors <color1> <color2> ...
  --parents-types <parent_type1> <parent_type2> ...
  --parents-names <parent_name11> <parent_name2> ...
  -i / --image-size <image_size>
  -s / --split-process
```

| Parameter       | Description                                                                                                                            | Default                                                   | Optional? |
| --------------- | -------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------- | --------- |
| `corpus`        | Name of the corpus from which the data will be retrieved.                                                                              |                                                           |           |
| `classes`       | List of element types that will be retrieved from Arkindex.                                                                            |                                                           |           |
| `colors`        | List of RGB colors that will be used in the label images. One color for each class. Example: `0,0,255 0,255,0` if there are 2 classes. |                                                           |           |
| `parents-types` | Type of parents of the elements. Example: `volume set`.                                                                                |                                                           |           |
| `parents-names` | Names of parents of the elements. Example: `train val test`.                                                                           | All children of elements in `parent-types` are retrieved. | &check;   |
| `image-size`    | Size in which the label images will be generated.                                                                                      | Generation at original image size.                        | &check;   |
| `split-process` | Whether to separate overlapping and touching boxes.                                                                                    | False                                                     | &check;   |

## Public annotations

In addition to the generation code, we provide the annotation images of the public datasets used during the experiments explained in the paper. The annotations of the training and validation images of the following datasets are available:
  - Bozen
  - DIVA-HisDB
  - HOME-NACR
  - Horae
  - READ (Complex and Simple tracks)
  - cBAD2019

## Citing us

```latex
@inproceedings{boillet2022,
    author = {Boillet, Mélodie and Kermorvant, Christopher and Paquet, Thierry},
    title = {{Robust text line detection in historical documents: learning and evaluation methods}},
    booktitle = {International Journal on Document Analysis and Recognition (IJDAR)},
    year = {2022},
    month = Mar,
    pages = {1433-2825},
    doi = {10.1007/s10032-022-00395-7},
    url = {https://doi.org/10.1007/s10032-022-00395-7}
}
```
